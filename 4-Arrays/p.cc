#include <iostream>

bool are_coplanar(const double v1[3], const double v2[3], const double v3[3]) {
	//     [ v1[0]  v1[1]  v1[2] ]
	// det [ v2[0]  v2[1]  v2[2] ] = 0 => coplanar
	//     [ v3[0]  v3[1]  v3[2] ]
	double det = v1[0]*v2[1]*v3[2] + v1[1]*v2[2]*v3[0]
	           + v1[2]*v2[0]*v3[1] - v1[2]*v2[1]*v3[0]
	           - v1[1]*v2[0]*v3[2] - v1[0]*v2[2]*v3[1];
	return det == 0;
}

class MatrixError {
public:
	int c1, r1, c2, r2;
	MatrixError	(int in_r1, int in_c1, int in_r2, int in_c2) {
		c1 = in_c1;
		r1 = in_r1;
		c2 = in_c2;
		r2 = in_r2;
	}
};

void print_mat_mult(
	const double* mat1, const double* mat2,
	int nrows1, int ncols1, int nrows2, int ncols2
	) {
	if (ncols1 != nrows2) throw MatrixError {nrows1, ncols1, nrows2, ncols2};
	for (int i {0}; i < nrows1; ++i) {
		for (int j {0}; j < ncols2; ++j) {
			double sum {0};
			for (int k {0}; k < ncols1; ++k) {
				sum += *(mat1 + i*ncols1 + k) *\
				*(mat2 + k*ncols2 + j);
			}
			std::cout << sum << '\t';
		}
		std::cout << '\n';
	}
}

int main() {
	double v1[] {1, 2, 3};
	double v2[] {4, 5, 6};
	double v3[] {7, 8, 9};

	std::cout << "Problem 1\n";
	std::cout << "Vectors v1, v2 and v3 are"
	          << (are_coplanar(v1, v2, v3) ? " " : " not ")
	          << "coplanar.\n\n";

	double m1[2][3] {{1, 2, 3}, {4, 5, 6}};
	double m2[2][2] {{7, 8}, {9, 10}};
	std::cout << "Problem 2\n";
	try {
		print_mat_mult(&m1[0][0], &m2[0][0], 2, 3, 2, 2);
	}
	catch (MatrixError& e) {
		std::cerr << "Dimension mismatch: [" << e.r1 << "x" << e.c1
		          << "] and [" << e.r2 << "x" << e.c2 << "].\n";
	}
}
