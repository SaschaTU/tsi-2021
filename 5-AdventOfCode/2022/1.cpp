#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

int main() {
	std::ifstream f{"1.dat"};
	unsigned long cur_cal {0};
	std::vector<unsigned long> snacks;
	for (std::string str; std::getline(f, str);) {
		if (str == "") {
			snacks.push_back(cur_cal);
			cur_cal = 0;
			continue;
		}
		cur_cal += std::stoul(str);
	}
	f.close();

	std::sort(snacks.begin(), snacks.end(), [](auto a, auto b){ return a > b; });

	unsigned long max_cal {0};
	for (std::size_t i {0}; i < 3; ++i)
		max_cal += snacks[i];

	std::cout << "Answer [1A]: " << snacks[0] << '\n';
	std::cout << "Answer [1B]: " << max_cal << '\n';
}