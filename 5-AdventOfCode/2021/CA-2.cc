#include <iostream>
#include <string>

int main() {
	std::string direction;
	unsigned step;
	unsigned curX {0}, curY {0};

	while (std::cin >> direction && std::cin >> step) {
		if (direction == "forward") curX += step;
		else if (direction == "down") curY += step;
		else if (direction == "up") curY -= step;
	}
	std::cout << curX * curY;
}