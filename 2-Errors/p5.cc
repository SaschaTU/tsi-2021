#include <iostream>
#include <vector>

int main() {
    const std::vector<int> sol {4, 1, 3, 2}; // Use randomisation in production.
    std::vector<int> guess(4);
    int bulls {0}, cows {0};

    while (bulls < 4) {
        bulls = 0;
        cows = 0;
        bool error {false};

        // User input
        std::cout << "Your guess (4 numbers [0-9]): ";
        for (int i {0}; i < 4; ++i)
            std::cin >> guess[i];

        // Basic input validation
        for (int i {0}; i < 4; ++i) {
            if (guess[i] < 0 || guess[i] > 9) {
                std::cerr << "Numbers should be from 0 to 9.\n\n";
                error = true;
                break;
            }
            for (int j {i+1}; j < 4; ++j) {
                if (guess[i] == guess[j]) {
                    std::cerr << "All numbers should be different.\n\n";
                    error = true;
                    break;
                }
            }
            if (error) break;
        }
        if (error) continue;
        
        for (int i {0}; i < 4; ++i) {
            for (int j {0}; j < 4; ++j) {
                if (guess[i] == sol[i]) {
                    ++bulls;
                    break;
                }
                else if(guess[i] == sol[j]) {
                    ++cows;
                    break;
                }
            }
        }
        std::cout << bulls << " bulls, " << cows << " cows.\n\n";
    }
}