#include <iostream>
#include <fstream>

unsigned res(unsigned in, unsigned out) {
	return (out + 1) + (out - in + 4) % 3 * 3;
}

int main() {
	std::ifstream f{"2.dat"};
	char in, out, out_b;
	unsigned res_a {0};
	unsigned res_b {0};
	while (f >> in >> out) {
		in -= 'A';
		out -= 'X';
		out_b = (in + (out + 2)) % 3;
		res_a += res(in, out);
		res_b += res(in, out_b);
	}

	std::cout << "Answer [2A]: " << res_a << '\n';
	std::cout << "Answer [1B]: " << res_b << '\n';
}