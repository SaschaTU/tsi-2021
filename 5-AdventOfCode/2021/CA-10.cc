#include <iostream>
#include <string>
#include <stack>

inline bool is_op(char c) {
	return c == '[' || c == '(' || c == '{' || c == '<';
}

char matching(char x) {
	switch (x) {
	case ']':
		return '[';
		break;
	case ')':
		return '(';
		break;
	case '}':
		return '{';
		break;
	case '>':
		return '<';
		break;
	default:
		return 0;
	};
}

int main () {
	unsigned sum {0};
	for (std::string line; std::cin >> line;) {
		std::stack<char> brackets {};
		for (auto& c : line) {
			if (is_op(c))
				brackets.push(c);
			else if (matching(c) == brackets.top())
				brackets.pop();
			else {
				sum += (c == ')') ? 3 :
					(c == ']') ? 57 :
					(c == '}') ? 1197 :
					(c == '>') ? 25137 : 0;
				break;
			}
		}
	}
}

// {([]<>)}
// 01234567
// {(<>)} i=3
// 012345
// {()} i=2
// 012345
