#include <iostream>

int main() {
	unsigned previous, current;
	unsigned counter {0};
	std::cin >> previous;

	while (std::cin >> current) {
		if (current > previous)
			++counter;
		previous = current;
	}

	std::cout << counter;
}