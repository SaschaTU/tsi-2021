#include <iostream>
#include <vector>
#include <string>

void print(std::vector<int>& v) {
    if (v.size() < 1) {
        std::cout << "(--)";
        return;
    }

    std::cout << "(" << v[0];
    for (int i {1}; i < v.size(); ++i) {
        std::cout << ", " << v[i];
    }
    std::cout << ")";
}

int total(std::vector<int>& v) {
    int sum {0};
    for (auto x : v)
        sum += x;
    return sum;
}

int main() {
    std::vector<int> mon;
    std::vector<int> tue;
    std::vector<int> wed;
    std::vector<int> thu;
    std::vector<int> fri;
    std::vector<int> sat;
    std::vector<int> sun;

    std::string day {""};
    int num {0};
    int rejected {0};

    std::cout << "Input (Day Num) pairs: ";
    while (std::cin >> day >> num) {
        if (day == "Monday" || day == "Mon")
            mon.push_back(num);
        else if (day == "Tuesday" || day == "Tue")
            tue.push_back(num);
        else if (day == "Wednesday" || day == "Wed")
            wed.push_back(num);
        else if (day == "Thursday" || day == "Thu")
            thu.push_back(num);
        else if (day == "Friday" || day == "Fri")
            fri.push_back(num);
        else if (day == "Saturday" || day == "Sat")
            sat.push_back(num);
        else if (day == "Sunday" || day == "Sun")
            sun.push_back(num);
        else
            ++rejected;
        std::cout << "Input (Day Num) pairs: ";
    }

    std::cout << "\nMon: ";
    print(mon);
    std::cout << " -- Total: " << total(mon);
    std::cout << "\nTue: ";
    print(tue);
    std::cout << " -- Total: " << total(tue);
    std::cout << "\nWed: ";
    print(wed);
    std::cout << " -- Total: " << total(wed);
    std::cout << "\nThu: ";
    print(thu);
    std::cout << " -- Total: " << total(thu);
    std::cout << "\nFri: ";
    print(fri);
    std::cout << " -- Total: " << total(fri);
    std::cout << "\nSat: ";
    print(sat);
    std::cout << " -- Total: " << total(sat);
    std::cout << "\nSun: ";
    print(sun);
    std::cout << " -- Total: " << total(sun);
    std::cout << "\n\nRejected: " << rejected << '\n';
}