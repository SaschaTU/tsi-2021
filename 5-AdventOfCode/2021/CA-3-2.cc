#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

unsigned str_to_bin(std::string& s) {
	unsigned res {0};
	for (auto& b : s) {
		res <<= 1;
		res += (b == '1');
	}
	return res;
}

int main() {
	std::vector<std::string> data;
	for (std::string code; std::cin >> code;)
		data.push_back(code);
	auto data_o2 {data};
	auto data_co2 {data};

	const int size = data[0].size();

	for (int bit {0}; bit < size; ++bit) {
		auto size_o2 {data_o2.size()};
		auto size_co2 {data_co2.size()};

		if (size_o2 <= 1 && size_co2 <= 1)
			break;

		// Oxygen
		if (size_o2 > 1) {
			int countOnes = std::count_if(
				data_o2.begin(), data_o2.end(), [=](auto& x) {
					return x[bit] == '1';
				});
			bool remove_o2 = 1;
			if (countOnes >= (data_o2.size() + 1)/2)
				remove_o2 = 0;
			data_o2.resize(std::distance(
				data_o2.begin(), std::remove_if(
					data_o2.begin(), data_o2.end(), [=](auto& x) {
						return (x[bit] == '0') ^ remove_o2;
					})
			));
		}

		// Carbon dioxide
		if (size_co2 > 1) {
			int countOnes = std::count_if(
				data_co2.begin(), data_co2.end(), [=](auto& x) {
					return x[bit] == '1';
				});
			bool remove_co2 = 1;
			if (countOnes < (data_co2.size() + 1)/2)
				remove_co2 = 0;
			data_co2.resize(std::distance(
				data_co2.begin(), std::remove_if(
					data_co2.begin(), data_co2.end(), [=](auto& x) {
						return (x[bit] == '0') ^ remove_co2;
					})
			));
		}
	}
	unsigned o2 = str_to_bin(data_o2[0]);
	unsigned co2 = str_to_bin(data_co2[0]);

	std::cout << "O2: " << data_o2[0] << "\nCO2: " << data_co2[0];
	std::cout << "\n\nO2: " << o2 << "\nCO2: " << co2;
	std::cout << "\n\nResult: " << o2 * co2;
}