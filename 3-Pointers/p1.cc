#include <iostream>
#include <string>

class BadFormat {};

int main() {
	int code_length {1};
	std::string full_number {""};
	std::cin >> full_number;

	try {
		if (full_number[0] != '+') throw BadFormat {};
		switch (full_number[1]) {
		case '1':
		case '7':
			code_length = 1;
			break;
		case '2':
			switch (full_number[2]) {
			case '0':
			case '7':
			case '8':
				code_length = 2;
				break;
			default:
				code_length = 3;
				break;
			}
			break;
		case '3':
			switch (full_number[2]) {
			case '5':
			case '7':
			case '8':
				code_length = 3;
				break;
			default:
				code_length = 2;
				break;
			}
			break;
		case '4':
			switch (full_number[2]) {
			case '2':
				code_length = 3;
				break;
			default:
				code_length = 2;
				break;
			}
			break;
		case '5':
			switch (full_number[2]) {
			case '0':
			case '9':
				code_length = 3;
				break;
			default:
				code_length = 2;
				break;
			}
			break;
		case '6':
			switch (full_number[2]) {
			case '7':
			case '8':
			case '9':
				code_length = 3;
				break;
			default:
				code_length = 2;
				break;
			}
			break;
		case '8':
			switch (full_number[2]) {
			case '0':
			case '5':
			case '7':
			case '8':
				code_length = 3;
				break;
			default:
				code_length = 2;
				break;
			}
			break;
		case '9':
			switch (full_number[2]) {
			case '6':
			case '7':
			case '9':
				code_length = 3;
				break;
			default:
				code_length = 2;
				break;
			}
			break;
		default:
			throw BadFormat {};
			break;
		}
		std::cout << "Country code: " << full_number.substr(1, code_length) << '\n';
		std::cout << "Local number: " << full_number.substr(code_length) << '\n';
	}
	catch (BadFormat) {
		std::cerr << "Bad input format.\n";
	}
}