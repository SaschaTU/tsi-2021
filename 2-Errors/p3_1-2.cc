#include <iostream>
#include <vector>
#include <limits>

int main() {
    std::cout << "Please enter the number of values you want to sum: ";
    int n;
    std::cin >> n;

    if (n <= 0) {
        std::cerr << "N should be strictly positive.\n";
        return 2;
    }

    std::cout << "Please enter some integers (press '|' to stop): ";
    std::vector<int> v;
    for (int x; std::cin >> x;) {
        v.push_back(x);
    }

    if (v.size() < 1) {
        std::cerr << "No data received.\n";
        return 3;
    }
    if (n > v.size()) {
        std::cerr << "N is greater than the number of values in the list.\n";
        return 1;
    }

    int sum {0};
    for (int i = 0; i < n; ++i) {
        if (std::numeric_limits<int>::max()-sum < v[i]) {
            std::cerr << "Overflow.\n";
            return 4;
        }
        sum += v[i];
    }
    
    std::cout << "The sum of first " << n << " numbers ( ";
    for (int i {0}; i < n; ++i)
        std::cout << v[i] << ' ';
    std::cout << ") is " << sum << ".\n";
}
