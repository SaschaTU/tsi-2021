#include <iostream>

int main() {
	double numbers[] {-0.1, 2., 4.4, 2.4, 4.};
	const int sz {sizeof(numbers) / sizeof(numbers[0])};
	// const int sz = std::size(numbers);
	double average {};
	for (int i {0}; i < sz; ++i) {
		average += numbers[i];
	}
	average /= sz;
	std::cout << "Average: " << average << "\n";

	constexpr int sz_fib {10};
	double fib[sz_fib];
	fib[0] = 1;
	fib[1] = 1;
	for (int i {2}; i < sz_fib; ++i) {
		fib[i] = fib[i-2] + fib[i-1];
	}
	std::cout << "The first " << sz_fib << " Fibonacci numbers are ";
	for (int i {0}; i < sz_fib; ++i) {
		if (i == 0) {
			std::cout << fib[i];
			continue;
		}
		std::cout << ", " << fib[i];
	}
	std::cout << ".\n";
}