#include <iostream>
#include <vector>
#include <string>
#include <stdexcept>

using namespace std;

int main() {
    try {
    	// Drill 1, 2, 25
    	cout << "Success!\n";
    	// Drill 3, 4
    	cout << "Success" << "!\n";
    	// Drill 5
    	int res = 7; vector<int> v5(10); v5[5] = res; cout << "Success!\n";
    	// Drill 6
    	vector<int> v6(10); v6[5] = 7; if (v6[5] == 7) cout << "Success!\n";
    	// Drill 7
    	bool cond = true; if (cond) cout << "Success!\n"; else cout << "Fail!\n";
    	// Drill 8
    	bool c8 = false; if (!c8) cout << "Success!\n"; else cout << "Fail!\n";
    	// Drill 9
    	string s9 = "ape"; bool c9 = "fool" > s9; if (c9) cout << "Success!\n";
    	// Drill 10, 11, 12
    	string s10 = "ape"; if (s10 != "fool") cout << "Success!\n";
    	// Drill 13, 14
    	vector<char> v13(5); for (int i = 0; i < v13.size(); ++i) ; cout << "Success!\n";
    	// Drill 15
    	string s15 = "Success!\n"; for (int i = 0; i < 9; ++i) cout << s15[i];
    	// Drill 16
    	if (true) cout << "Success!\n"; else cout << "Fail!\n";
    	// Drill 17
    	int x17 = 2000; char c17 = x17; if (c17 == -48) cout << "Success!\n";
    	// Drill 18
    	string s18 = "Success!\n"; for (int i = 0; i < 9; ++i) cout << s18[i];
    	// Drill 19
    	vector<int> v19(5); for (int i = 0; i < v19.size(); ++i) ; cout << "Success!\n";
    	// Drill 20, 23
    	int i20 = 0; int j20 = 9; while (i20 < 10) ++i20; if (j20 < i20) cout << "Success!\n";
    	// Drill 21
    	int x21 = 2; double d21 = 5. / (x21 - 2.); if (d21 != 2*x21 + 0.5) cout << "Success!\n";
    	// Drill 22
    	string s22 = "Success!\n"; for (int i = 0; i < 10; ++i) cout << s22[i];
    	// Drill 24
    	int x24 = 4; double d24 = 5. / (x24 - 2.); if (d24 == 5. / (x24 - 2.)) cout << "Success!\n";

        return 0;
    }
    catch (exception& e) {
        cerr << "Error: " << e.what() << '\n'; 
        return 1;
    }
    catch (...) {
        cerr << "Oops: unknown exception!\n"; 
        return 2;
    }
}