#include <iostream>
#include <cmath>

int main() {
    double a, b, c;
    std::cout << "ax² + bx + c = 0:\n";
    std::cout << "    a = ";
    std::cin >> a;
    std::cout << "    b = ";
    std::cin >> b;
    std::cout << "    c = ";
    std::cin >> c;
    
    double d = b * b - 4. * a * c;
    if (d < 0) {
        std::cerr << "No real roots.\n";
        return 1;
    }
    double x1 = (-b + std::sqrt(d))/2./a;
    double x2 = (-b - std::sqrt(d))/2./a;
    std::cout << "x₁ = " << x1 << '\n';
    std::cout << "x₂ = " << x2 << '\n';
}
