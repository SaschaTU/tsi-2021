#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

int main() {
	std::vector<unsigned> draws;

	for (unsigned draw; std::cin >> draw;) {
		draws.push_back(draw);
		if (std::cin.peek() == ',')
			std::cin.ignore();
		else if (std::cin.peek() == '\n')
			break;
	}

	std::vector<unsigned> board;
	for (unsigned field; std::cin >> field;)
		board.push_back(field);
	
	const unsigned n_boards = board.size() / 25;
	std::vector<bool> matches(board.size(), false);
	
	for (auto& draw : draws) {
		for (int i {0}; i < board.size(); ++i)
			if (board[i] == draw)
				matches[i] = true;
		for (int i {0}; i < n_boards; ++i) {
			for (int row {0}; row < 5; ++row) {
				for (int col {0}; col < 5; ++col) {
					bool cur_match = matches[i*25+row*5+col];
					if (!cur_match) break;
					if (col == 4) {
						std::cout << "Board " << i << " won.\n";
						unsigned sum {0};
						for (int j {0}; j < 25; ++j) {
							if (!matches[25*i+j])
								sum += board[25*i+j];
						}
						std::cout << "Result: " << sum * draw;
						return 0;
					}
				}
			}
			for (int col {0}; col < 5; ++col) {
				for (int row {0}; row < 5; ++row) {
					bool cur_match = matches[i*25+row*5+col];
					if (!cur_match) break;
					if (row == 4) {
						std::cout << "Board " << i << " won.\n";
						unsigned sum {0};
						for (int j {0}; j < 25; ++j) {
							if (!matches[25*i+j])
								sum += board[25*i+j];
						}
						std::cout << "Result: " << sum * draw;
						return 0;
					}
				}
			}
		}
	}
}