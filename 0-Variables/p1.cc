#include <iostream>

int main() {
	std::cout << "Size of bool is " << sizeof(bool) << " byte(s).\n";
	std::cout << "Size of char is " << sizeof(char) << " byte(s).\n";
	std::cout << "Size of int is " << sizeof(int) << " byte(s).\n";
	std::cout << "Size of float is " << sizeof(float) << " byte(s).\n";
	std::cout << "Size of double is " << sizeof(double) << " byte(s).\n";
}