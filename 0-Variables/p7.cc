#include <iostream>

int main() {
	std::cout << "Enter three integer values: ";
	int val1, val2, val3;
	std::cin >> val1 >> val2 >> val3;
	if (val2 < val1) {
		int temp = val2;
		val2 = val1;
		val1 = temp;
	}
	if (val3 < val2) {
		int temp = val3;
		val3 = val2;
		val2 = temp;
	}
	if (val2 < val1) {
		int temp = val2;
		val2 = val1;
		val1 = temp;
	}
	std::cout << val1 << ", " << val2 << ", " << val3 << '\n';

	// Cleaner alternative using std::vector
	// For that you should:
	//   #include <vector>
	//   #include <algorithm>

	// std::vector v {val1, val2, val3};
	// std::sort(v.begin(), v.end());
	// std::cout << v[0] << ", " << v[1] << ", " << v[2] << '\n';
}