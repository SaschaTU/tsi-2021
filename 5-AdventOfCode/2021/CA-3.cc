#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

int main() {
	std::vector<std::string> data;
	for (std::string code; std::cin >> code;)
		data.push_back(code);

	const int size = data[0].size();
	unsigned gamma {0}, epsilon {0};

	for (int bit {0}; bit < size; ++bit) {
		int countOnes = std::count_if(
			data.begin(), data.end(), [=](auto& x) {
				return x[bit] == '1';
			});
		gamma <<= 1;
		if (countOnes >= (data.size() + 1)/2)
			++gamma;
	}
	epsilon = gamma ^ ((1 << size) - 1);

	std::cout << "G: " << gamma << "\nE: " << epsilon;
	std::cout << "\nResult: " <<gamma * epsilon;
}