#include <iostream>

int main() {
	std::cout << "Enter two integer values: ";
	int val1, val2;
	std::cin >> val1 >> val2;
	std::cout << "max(" << val1 << ", " << val2 << ") = "
	          << (val1 > val2 ? val1 : val2) << '\n';
	std::cout << "min(" << val1 << ", " << val2 << ") = "
	          << (val2 > val1 ? val1 : val2) << '\n';
	std::cout << val1 << " + " << val2 << " = "
	          << (val1 + val2) << '\n';
	std::cout << val1 << " - " << val2 << " = "
	          << (val1 - val2) << '\n';
	std::cout << val1 << " * " << val2 << " = "
	          << (val1 * val2) << '\n';
	std::cout << val1 << " / " << val2 << " = "
	          << (val1 / val2) << '\n';
}