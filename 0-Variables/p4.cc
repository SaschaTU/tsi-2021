#include <iostream>

int main() {
	std::cout << "Enter your age: ";
	int age;
	std::cin >> age;
	std::cout << "I hear you just had a birthday and you are "
	          << age << " years old.\n";
	if (age < 0 || age > 110) {
		std::cout << "You must be kidding!\n";
	}
}