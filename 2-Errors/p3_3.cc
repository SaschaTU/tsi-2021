#include <iostream>
#include <vector>

int main() {
    std::cout << "Please enter the number of values you want to sum: ";
    int n;
    std::cin >> n;

    if (n <= 0) {
        std::cerr << "N should be strictly positive.\n";
        return 2;
    }

    std::cout << "Please enter some floats (press '|' to stop): ";
    std::vector<double> v;
    for (double x; std::cin >> x;) v.push_back(x);

    if (v.size() < 1) {
        std::cerr << "No data received.\n";
        return 3;
    }
    if (n > v.size()) {
        std::cerr << "N is greater than the number of values in the list.\n";
        return 1;
    }

    double sum {0};
    for (int i = 0; i < n; ++i) sum += v[i];

    std::vector<double> diffs;
    for (int i = 1; i < v.size(); ++i) diffs.push_back(v[i] - v[i-1]);
    
    std::cout << "The sum of first " << n << " numbers ( ";
    for (int i {0}; i < n; ++i)
        std::cout << v[i] << ' ';
    std::cout << ") is " << sum << ".\n";

    std::cout << "The differences are ( ";
    for (auto e : diffs) std::cout << e << ' ';
    std::cout << ").\n";
}
