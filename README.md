# Introduction to programming

Here you can find the problems from drills, practice sessions and in-class demonstrations of the course _Introduction to programming_ read in 2021/2022. The code is groupped by topics and each topic roughly corresponds to one week (4 ac. h.) of lectures.

The names of files start with `p` for problems, `d` for drills or `l` for demonstrations during the lectures.

## Topics

0. Variables and types
1. Computation
2. Errors