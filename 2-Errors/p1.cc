#include <iostream>

using namespace std;

class Range_error {};

double c_to_k(double c) {
    constexpr double abs_zero = -273.15;
    if (c < abs_zero) {
        cerr << "Temperature below absolute zero.\n";
        throw Range_error {};
    }
    double k = c + 273.15;
    return k;
}

double k_to_c(double k) {
    if (k < 0) {
        cerr << "Temperature below absolute zero.\n";
        throw Range_error {};
    }
    constexpr double abs_zero = -273.15;
    return k + abs_zero;
}

double c_to_f(double c) {
    return c * 9./5. + 32.;
}

double f_to_c(double f) {
    return (f - 32.) * 5./9.;
}

int main() {
    double x = 0;
    cin >> x;
    double k = c_to_k(x);
    double c = k_to_c(x);
    double f = c_to_f(x);
    double c1 = f_to_c(x);
    cout << '\n';
    cout << x << " °C = " << k << " K\n";
    cout << x << " K = " << c << " °C\n";
    cout << '\n';
    cout << x << " °C = " << f << " °F\n";
    cout << x << " °F = " << c1 << " °C\n";
}
