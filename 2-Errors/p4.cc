#include <iostream>
#include <vector>
#include <limits>

// A much more compact implementation would use recursion.
std::vector<int> fib(int n) {
    std::vector<int> res;
    if (n > 0) {
        res.push_back(1);
    }
    if (n > 1) {
        res.push_back(1);
    }

    int xm2 {1}, xm1 {1};
    for (int i = 2; i < n; ++i) {
        if (std::numeric_limits<int>::max() - xm1 < xm2) {
            std::cout << "\nMaximal Fibonacci number to fit into an int is " << xm1 << "\n\n";
            break;
        }
        int x = xm1 + xm2;
        xm2 = xm1;
        xm1 = x;
        res.push_back(x);
    }
    return res;
}

int main() {
    // Enter something reasonably big to get
    // the max Fibonacci number to fit into int.
    std::cout << "Please enter the number of Fibonacci numbers you wish to get: ";
    int n;
    std::cin >> n;

    std::vector<int> f = fib(n);
    for (auto x : f) std::cout << x << ' ';
}