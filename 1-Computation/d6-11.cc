#include <iostream>
#include <limits>
#include <string>
#include <vector>
#include <algorithm>

int main() {
	// Drill 6
	// std::cout << "Input a number: ";
	// double a;
	// double min = std::numeric_limits<double>::max();
	// double max = -std::numeric_limits<double>::max();

	// while(std::cin >> a) {
		
	// 	std::cout << "\na = " << a;
	// 	if (a < min) {
	// 		min = a;
	// 		std::cout << " -- the smallest so far";
	// 	}
	// 	if (a > max) {
	// 		max = a;
	// 		std::cout << " -- the largest so far";
	// 	}
	// 	std::cout << "\n\nInput a number: ";
	// }

	// Drills 7–11
	std::cout << "Input a number with units: ";
	double a;
	std::string unit;
	double min = std::numeric_limits<double>::max();
	double max = -std::numeric_limits<double>::max();
	int num_vals = 0;
	double sum = 0.;
	std::vector<double> values;

	constexpr double cm_to_m = 0.01;
	constexpr double in_to_m = 2.54 * cm_to_m;
	constexpr double ft_to_m = 12 * in_to_m;

	while (std::cin >> a >> unit) {
		if (unit != "cm" && unit != "in" && unit != "ft" && unit != "m") {
			std::cerr << "\nWrong units\n\n";
		} else {
			double a_in_m = a;
			if (unit == "cm")
				a_in_m *= cm_to_m;
			if (unit == "in")
				a_in_m *= in_to_m;
			if (unit == "ft")
				a_in_m *= ft_to_m;

			if (a_in_m < min) min = a_in_m;
			if (a_in_m > max) max = a_in_m;

			sum += a_in_m;
			++num_vals;
			values.push_back(a_in_m);
		}
		std::cout << "Input a number with units: ";
	}

	std::cout << "\nMax length is " << max << " m";
	std::cout << "\nMin length is " << min << " m";
	std::cout << '\n' << num_vals << " measurements";
	std::cout << "\nTotal length is " << sum << " m";

	std::sort(values.begin(), values.end());
	std::cout << "\n\nSorted values: \n";
	for (auto value : values) {
		std::cout << '\t' << value << " m\n";
	}
}
