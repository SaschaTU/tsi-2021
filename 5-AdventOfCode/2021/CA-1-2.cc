#include <iostream>
#include <vector>

int main() {
	std::vector<unsigned> data;
	for (unsigned number; std::cin >> number;)
		data.push_back(number);

	const int w_size {3};
	unsigned previous {0};
	unsigned current {0};
	unsigned counter {0};
	for (int i {0}; i <= data.size() - w_size; ++i) {
		current = 0;
		for (int j {0}; j < w_size; ++j)
			current += data[i+j];

		if (i && current > previous)
			++counter;
		previous = current;
	}
	std::cout << counter;
}