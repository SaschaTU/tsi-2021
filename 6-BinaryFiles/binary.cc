#include <iostream>
#include <fstream>
#include <string>
#include <exception>
#include <vector>
#include <cstring>

struct Transaction {
	uint64_t id; // Transaction id
	std::string iban; // Beneficiary account
	uint32_t amount; // Transferred amount (in cents)
	std::string ref; // Reference or information
};

/**
 * Write a list of transactions into a file according to the format.
 * 
 * The format of the file is as follows.
 * +-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+
 * | 0x00        | 0x01        | 0x02        | 0x03        | 0x04        | 0x05        | 0x06        | 0x07        |
 * +=============+=============+=============+=============+=============+=============+=============+=============+
 * | SIGNATURE TXS                           | MAJ VER 1   | .           | MIN VER 0   | NO OF TRANSACTIONS        |
 * +-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+
 *   For each Transaction:
 * +-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+
 * | TRANSACTION ID                                                                                                |
 * +-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+
 * | TRANSACTION AMOUNT                                    | NIBAN       | IBAN (N_IBAN bytes)...                  |
 * +-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+
 * | NREF        | REFERENCE (NREF bytes) ...|
 * +-------------+-------------+-------------+-------------+-------------+-------------+-------------+-------------+
 * 
 * @param payments [in] a list of payments.
 */
void write_tx(std::vector<Transaction>& transactions, std::string fname="data.dat") {
	if (std::ofstream ofs {fname, std::ios::binary}) {
		ofs.write("TXS", 3); // Signature
		ofs.write("1.0", 3); // Ver. 1.0
		uint16_t sz = transactions.size() < 65536 ? transactions.size() : 65535;
		ofs.write(reinterpret_cast<char*>(&sz), 2);
		for (auto& tx : transactions) {
			ofs.write(reinterpret_cast<char*>(&tx.id), 8);
			ofs.write(reinterpret_cast<char*>(&tx.amount), 4);
			uint8_t niban = tx.iban.size() < 255 ? tx.iban.size() : 255;
			uint8_t nref = tx.ref.size() < 255 ? tx.ref.size() : 255;
			ofs.write(reinterpret_cast<char*>(&niban), 1);
			ofs.write(tx.iban.c_str(), niban);
			ofs.write(reinterpret_cast<char*>(&nref), 1);
			ofs.write(tx.ref.c_str(), nref);
		}
	}
	else throw std::runtime_error("Could not open the file.");
}

std::vector<Transaction> read_tx(std::string fname="data.dat") {
	if (std::ifstream ifs {fname, std::ios::binary}) {
		char sig[4];
		char ver[4];
		ifs.read(sig, 3);
		ifs.read(ver, 3);
		sig[3] = '\0';
		ver[3] = '\0';
		if (std::string{sig} != "TXS") {
			throw std::runtime_error("Wrong file format.");
		}
		std::cerr << "Version: " << ver << '\n';
		uint16_t sz;
		ifs.read(reinterpret_cast<char*>(&sz), 2);

		std::vector<Transaction> txs(sz);
		for (auto& tx : txs) {
			ifs.read(reinterpret_cast<char*>(&tx.id), 8);
			ifs.read(reinterpret_cast<char*>(&tx.amount), 4);

			uint8_t niban;
			ifs.read(reinterpret_cast<char*>(&niban), 1);
			char* ciban = new char[niban+1];
			ifs.read(ciban, niban);
			ciban[niban] = '\0';
			tx.iban = ciban;

			uint8_t nref;
			ifs.read(reinterpret_cast<char*>(&nref), 1);
			char* cref = new char[nref+1];
			ifs.read(cref, nref);
			cref[nref] = '\0';
			tx.ref = cref;
		}

		return txs;
	}
	else throw std::runtime_error("Could not open the file.");
}

std::ostream& operator<<(std::ostream& os, const Transaction& tx) {
	return os << "TX " << tx.id << '\n'
	          << "IBAN:      " << tx.iban << '\n'
	          << "AMOUNT:    " << tx.amount << '\n'
	          << "REFERENCE: " << tx.ref << '\n';
}

int main() {
	// std::vector<Transaction> current {
	// 	{0, "LV11HABA0010224216649", 6499, "INVOICE 10266/12.12.2021"},
	// 	{1, "LV12HABA0010224226649", 500, "FISH AND CHIPS"},
	// 	{2, "LV13HABA0010224236649", 12000, "RENT JAN 2022"},
	// 	{3, "LV14HABA0010224246649", 1167, "2462646444/10122"},
	// };
	// write_tx(current);
	auto loaded = read_tx();
	std::cout << '\n';

	for (const auto& tx : loaded) {
		std::cout << tx << '\n';
	}
}