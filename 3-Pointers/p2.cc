#include <iostream>
#include <string>
#include <vector>

// Extract digits from the number, right-to-left
std::vector<int> rtl_digits(unsigned long num) {
	std::vector<int> res;
	for (int i {0}; num > 0; ++i, num /= 10)
		res.push_back(num % 10);
	return res;
}

// Validate a credit card number (Luhn's algorithm)
bool validatePAN(unsigned long pan) {
	auto digits = rtl_digits(pan);
	for (int i {1}; i < digits.size(); i += 2)
		digits[i] *= 2;

	int sum_digits {0};
	for (auto& d : digits){
		for (auto& dd : rtl_digits(d))
			sum_digits += dd;
	}

	return !(sum_digits % 10);
}

int main() {
	unsigned long pan;
	std::cout << "Credit card number: ";
	std::cin >> pan;

	std::cout << '\n' << pan << " ... ";
	if (validatePAN(pan))
		std::cout << "[VALID]\n";
	else
		std::cout << "[INVALID]\n";
}