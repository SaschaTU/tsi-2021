#include <iostream>
#include <cmath>

int main() {
	std::cout << "Input two numbers: ";
	// Drill 4
	double a, b;

	while(std::cin >> a >> b) {
		
		// Drill 5
		std::cout << "\nThe smaller value is: " << (a < b ? a : b);
		std::cout << "\nThe larger value is: " << (a > b ? a : b);

		if (std::abs(a - b) < 0.01) {
			std::cout << "\nThe numbers are almost equal";
		}

		std::cout << "\n\nInput two numbers: ";
	}
}