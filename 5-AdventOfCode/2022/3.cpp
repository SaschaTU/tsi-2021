#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>

char val(char c) {
	if (c >= 'a' && c <= 'z')
		return c - 'a' + 1;
	if (c >= 'A' && c <= 'Z')
		return c - 'A' + 27;
	return 0;
}

char common(std::vector<std::string> rucksacks) {
	std::set<char> res {rucksacks[0].begin(), rucksacks[0].end()};
	for (const auto& rucksack : rucksacks) {
		std::set<char> temp_res{};
		for (const auto& item : rucksack) 
			if (res.count(item))
				temp_res.insert(item);
		res = temp_res;
	}
	return *res.begin();
}

int main() {
	std::ifstream f{"temp.dat"};
	std::vector<std::string> rucksacks;
	unsigned res_a {0};
	unsigned res_b {0};

	for (std::string rucksack; f >> rucksack;)
		rucksacks.push_back(rucksack);

	for (const auto& rucksack : rucksacks) {
		std::size_t half = rucksack.size()/2;
		auto rucksack_l = rucksack.substr(0, half);
		auto rucksack_r = rucksack.substr(half);
		res_a += val(common({rucksack_l, rucksack_r}));
	}

	for (size_t i {0}; i < rucksacks.size(); i += 3)
		res_b += val(common({rucksacks[i], rucksacks[i+1], rucksacks[i+2]}));


	std::cout << "Answer [3A]: " << res_a << '\n';
	std::cout << "Answer [1B]: " << res_b << '\n';
}