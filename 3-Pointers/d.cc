#include <iostream>
#include <iomanip>

int main () {
	// Task 1
	char c {'Q'};
	// Task 2
	char* pc = &c;
	// Task 3
	std::cout << "Value:        '" << c << "'\n";
	std::cout << "Address:      " << static_cast<void*>(&c) << '\n';
	std::cout << "Pointer:      " << static_cast<void*>(pc) << '\n';
	std::cout << "Dereferenced: '" << *pc << "'\n\n";
	// Task 4
	for (int i {0}; i < 4; ++i) {
		std::cout << std::hex << std::setfill('0')
		          << std::setw(2) << int(*(pc+i)) << ' ';
	}
	std::cout << "\n\n";
	// Task 5
	*(pc+1) = 0xff;
	// Task 6
	double d {6.02e23};
	// Task 7
	double* pd = &d;
	std::cout << "double:    " << *pd << '\n';
	std::cout << "char:      " << *(reinterpret_cast<char*>(pd)) << '\n';
	std::cout << "int:       " << *(reinterpret_cast<int*>(pd)) << '\n';
	std::cout << "unsigned:  " << *(reinterpret_cast<unsigned*>(pd)) << '\n';
	std::cout << "float:     " << *(reinterpret_cast<float*>(pd)) << "\n\n";

	// & — get an address of the variable => unsigned long
	// pointer — an address and a type => int*
	// * — get the object at the address pointed to by the pointer

	// Task 8
	int x {1};
	int y {2};
	const int z {0};
	const int w {-1};

	int * p1 = &x;
	const int* p2 = &z; // points to const int
	// int const* p2 = &z; // points to const int
	int * const p3 = &x; // points to int
	const int* const p4 = &z;

	p1 = &y;
	std::cout << "*p1 = " << *p1 << '\n';
	// p1 = &z;
	// std::cout << "*p1 = " << *p1 << '\n';

	p2 = &w;
	std::cout << "*p2 = " << *p2 << '\n';
	p2 = &x;
	std::cout << "*p2 = " << *p2 << '\n';

	// p3 = &y;
	// std::cout << "*p3 = " << *p3 << '\n';
}

