#include <iostream>

int main() {
	std::cout << "Input two numbers: ";
	int a, b;

	while(std::cin >> a >> b) {

		// Drill 1
		// std::cout << "\na = " << a << "\nb = " << b;

		// Drill 2
		// std::cout << "\nThe smaller value is: " << (a < b ? a : b);
		// std::cout << "\nThe larger value is: " << (a > b ? a : b);

		// Drill 3
		if (a == b) {
			std::cout << "\nThe numbers are equal";
		} else {
			std::cout << "\nThe smaller value is: " << (a < b ? a : b);
			std::cout << "\nThe larger value is: " << (a > b ? a : b);
		}

		std::cout << "\n\nInput two numbers: ";
	}
}