#include <iostream>
#include <string>

int main() {
	std::string direction;
	unsigned step;
	unsigned curX {0}, curY {0};
	unsigned aim {0};

	while (std::cin >> direction && std::cin >> step) {
		if (direction == "forward") {
			curX += step;
			curY += aim * step;
		}
		else if (direction == "down") aim += step;
		else if (direction == "up") aim -= step;
	}
	std::cout << curX * curY;
}