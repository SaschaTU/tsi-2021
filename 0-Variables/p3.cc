#include <iostream>

int main() {
	double x;
	std::cout << "Input an float: x = ";
	std::cin >> x;

	int i = x;
	int ii {x}; // This produces a warning

	std::cout << " i = " << i << '\n';
	std::cout << "ii = " << i << '\n';

	/* Testing:

	   x = 15
	   i = ii = 15

	   x = 1.2
	   i = ii = 1
   */
}