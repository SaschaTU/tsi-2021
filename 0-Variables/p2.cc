#include <iostream>

int main() {
	int x;
	std::cout << "Input an integer: x = ";
	std::cin >> x;

	unsigned u = x;
	short s = x;
	char c = x;
	char ci {x}; // This produces a warning

	std::cout << " u = " << u << '\n';
	std::cout << " s = " << s << '\n';
	std::cout << " c = " << int(c) << '\n';
	std::cout << "ci = " << int(ci) << '\n';

	/* Testing:
	  
	   x = 15
	   u = s = c = ci = 15

	   x = -52
	   u = 4294967244
	   s = c = ci = -52

	   x = 1000
	   u = s = 1000
	   c = ci = -24

	   x = -123456789
	   u = 4171510507
	   s = 13035
	   c = ci = -21
	*/
}